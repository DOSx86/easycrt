(*
   	EasyCRT, version 0.1
	A drop-in replacement unit for Turbo Pascal's CRT unit.

	BSD 3-Clause License
	Copyright (c) 2022, Jerome Shidel
	All rights reserved.
*)

unit EasyCRT;

{$G-}       (* 8086/8087 compatible *)
{$A+,B-}    (* Byte alignment, short-circut boolean *)
{$E-,N-}    (* Emulation, coprocessor *)
{$F+,O-}    (* Farcalls, and no overlays *)
{$R-,Q-,S-} (* No range, overflow or stack checking *)
{$I-}       (* No I/O checking *)
{$D-,L-,Y-} (* No Debug, label or symbol information *)
{$P-,V+}    (* OpenString parameters, with strict type-checking *)
{$T-}       (* No type-checked pointers *)
{$X+}       (* Enable extended syntax *)

{$DEFINE Extras} (* Build with extra functions and other general improvements *)

{$IFOPT G+}
{$DEFINE IntTimer} (* Use BIOS high-precision INT instead of ticks for timer *)
{$ENDIF}

interface

const
    { CRT modes }
    BW40          = 0;      { 40x25 B/W on Color adapter }
    CO40          = 1;      { 40x25 Color on Color adapter }
    BW80          = 2;      { 80x25 B/W on Color adapter }
    CO80          = 3;      { 80x25 Color on Color adapter }
    Mono          = 7;      { 80x25 on Monochrome adapter }
    Font8x8       = $0100;  { Add-in for 8x8 ROM font }
{$IFDEF Extras}
    Font8x14      = $0200;  { Add-in for 8x14 ROM font, EGA and VGA }
    Font8x16      = $0300;  { Add-in for 8x16 ROM font, VGA only }
{$ENDIF}

    { Mode constants for 3.0 compatibility }
    C40           = CO40;
    C80           = CO80;

    { Foreground and background color constants }
    Black         = 0;
    Blue          = 1;
    Green         = 2;
    Cyan          = 3;
    Red           = 4;
    Magenta       = 5;
    Brown         = 6;
    LightGray     = 7;

    { Foreground color constants }
    DarkGray      = 8;
    LightBlue     = 9;
    LightGreen    = 10;
    LightCyan     = 11;
    LightRed      = 12;
    LightMagenta  = 13;
    Yellow        = 14;
    White         = 15;

  { Add-in for blinking and intense background constants }
    Blink         = $80;
{$IFDEF Extras}
    Intensity     = Blink;
{$ENDIF}

{$IFDEF Extras}
  { TTL Monochrome attributes constants }
    Underline               = $01;
    Normal                  = $07;
    BrightUnderline         = $09;
    Bold                    = $0F;
    Reverse                 = $70;
    BlinkingUnderline       = $81;
    BlinkingNormal          = $87;
    BlinkingBrightUnderline = $89;
    BlinkingBold            = $8F;
{$ENDIF}

{$IFDEF Extras}
  { One-Color composite attributes constants }
  { Normal                  = $07; }
    GrayOnBlack             = $08;
  { Bold                    = $0F; }
  { Reverse                 = $70; }
    GrayOnWhite             = $78;
    WhiteOnWhite            = $7F;
  { BlinkingNormal          = $87; }
  { BlinkingBold            = $8F; }
{$ENDIF}

var

{ Standard Interface Variables }

{ At Present, CheckBreak, CheckEOF, CheckSnow and DirectVideo are not supported.
  Eventually, I'll get around to adding support. But, not now. }

    CheckBreak  : boolean;  { Enable Ctrl-Break }
    CheckEOF    : boolean;  { Enable Ctrl-Z }
    DirectVideo : boolean;  { Enable direct video addressing }
    CheckSnow   : boolean;  { Enable snow filtering }
    LastMode    : word;     { Current text mode }
    TextAttr    : byte;     { Current text attribute }
    WindMin     : word;     { Window upper left coordinates }
    WindMax     : word;     { Window lower right coordinates }

{ Standard procedures and functions }
procedure AssignCrt(var F : text);
function KeyPressed : boolean;
function ReadKey : char;
procedure TextMode(Mode : word);
procedure Window(X1, Y1, X2, Y2 : byte);
procedure GotoXY(X, Y : byte);
function WhereX: byte;
function WhereY: byte;
procedure ClrScr;
procedure ClrEol;
procedure InsLine;
procedure DelLine;
procedure TextColor(Color : byte);
procedure TextBackground(Color : byte);
procedure LowVideo;
procedure HighVideo;
procedure NormVideo;
procedure Delay(MS : word);
procedure Sound(Hz : word);
procedure NoSound;

implementation

var
    OldExitProc : Pointer;
    ActivePage : byte;
    FirstAttr : byte;
{$IFDEF IntTimer}
	DelayData	: byte;
{$ENDIF}

procedure CarriageReturn;
begin
	GotoXY(1,WhereY);
end;

procedure LineFeed;
var
	Y : byte;
begin
	Y := WhereY + 1;
	if Y > Hi(WindMax) - Hi(WindMin) then asm
		mov		ax, $0601
		mov		bh, TextAttr
		mov		cx, WindMin
		mov		dx, WindMax
		sub		cx, $0101
		sub		dx, $0101
		int		$10

	end else
		GotoXY(WhereX, Y);
end;

procedure BackSpace;
begin
	if WhereX > 1 then
		GotoXY(WhereX - 1, WhereY);
end;

procedure Tab; begin end;

function ReadTextAttr : word; assembler;
asm
	mov		ah, $08
	mov		bh, ActivePage
	int		$10
end;

procedure WriteTextAttr(C : Char); assembler;
asm
	mov		ah, $09
	mov		al, C
	mov		bh, ActivePage
	mov		bl, TextAttr
	mov		cx, 1
	int		$10
end;

procedure WriteCRT(C : Char);
var
	X : Byte;
begin
	case byte(C) of
		$0A : LineFeed;
		$0D : CarriageReturn;
		$08 : BackSpace;
	else
		WriteTextAttr(C);
		X := WhereX;
		Inc(X);
		if X > Lo(WindMax) - Lo(WindMin) then begin
			CarriageReturn;
			LineFeed;
		end else
			GotoXY(X, WhereY);
	end;
end;

const
    { internal file mode constants }
    fmClosed           = $D7B0;
    fmInput            = $D7B1;
    fmOutput           = $D7B2;
    fmInOut            = $D7B3;

{ System Read & Write IO Redirection }
type
    PTextBuf = ^TTextBuf;
    TTextBuf = array [0..127] of char;
    TTextName = array [0..79] of char;
    TTextRec = record
      Handle    : word;
      Mode      : word;
      BufSize   : word;
      Private   : word;
      BufPos    : word;
      BufEnd    : word;
      BufPtr    : PTextBuf;
      OpenFunc  : function (var F) : integer;
      InOutFunc : function (var F) : integer;
      FlushFunc : function (var F) : integer;
      CloseFunc : function (var F) : integer;
      UserData  : array [1..16] of byte;
      Name      : TTextName;
      Buffer    : TTextBuf;
    end;

function ReadCRT(var BufPtr : PTextBuf; Size : word) : word;
begin
    ReadCRT := 0;
end;

function InOutCRT(var F) : integer;
var
    I  : word;
begin
    with TTextRec(F) do
    case Mode of
        $D7B1 : begin {fmInput}
             BufEnd := ReadCRT(BufPtr, BufSize);
             BufPos := 0;
        end;
        $D7B2 : begin {fmOutput}
            I := BufEnd;
            while I < BufPos do begin
            	WriteCRT(BufPtr^[I]);
                Inc(I);
                if I >= BufSize then I := 0;
            end;
            BufPos := 0;
        end; { * }
    end;
    InOutCrt := 0;
end;

function FlushCRT(var F) : integer;
var
    Ignored : integer;
begin
    case TTextRec(F).Mode of
        fmOutput : Ignored := InOutCrt(F);
        fmInput : begin
            TTextRec(F).BufEnd := 0;
            TTextRec(F).BufPos := 0;
        end;
    end;
    FlushCrt := 0;
end;

function CloseCRT(var F) : integer;
begin
    TTextRec(F).Mode := fmClosed;
    CloseCrt := 0;
end;

function OpenCRT(var F) : integer;
begin
    with TTextRec(F) do
        begin
            if Mode = fmInOut then
                Mode := fmOutput;
        end;
    OpenCrt := 0;
end;

procedure UnitFinalize;
begin
    ExitProc := OldExitProc;
    Assign(Output, '');
    Assign(Input, '');
    ReWrite(Output);
    Reset(Input);
end;

procedure InitVideoData; assembler;
asm
	mov		CheckSnow, True
	mov		DirectVideo, True

	mov		WindMin, $0101
	mov		ax, $0040
	push	ax
	pop		es
	mov		ah, [es:$0084]
	inc		ah
	mov		al, [es:$004a]
	mov		WindMax, ax
	mov		ah, $0f
	int		$10
	mov		ActivePage, bh
	xor		ah, ah
	mov		LastMode, ax

	mov		ah, $08
	mov		bh, ActivePage
	int		$10
	mov		TextAttr, ah
end;

procedure UnitInitialize;
begin
    OldExitProc := ExitProc;
    ExitProc := @UnitFinalize;
    FirstAttr   := Hi(ReadTextAttr);
    CheckBreak  := True;
    CheckEOF    := False;
    InitVideoData;
    AssignCrt(Output);
    Rewrite(Output);
{   AssignCRT(Input);
    Reset(Input); }
end;

procedure AssignCrt(var F : text);
const
    CrtNameID : array [0..4] of char = ('C', 'R', 'T', #0, #0);
begin
    with TTextRec(F) do begin
        Handle      := $FFFF;
        Mode        := fmClosed;
        BufSize     := Sizeof(Buffer);
        BufPtr      := @Buffer;
        OpenFunc    := OpenCrt;
        InOutFunc   := InOutCrt;
        FlushFunc   := FlushCrt;
        CloseFunc   := CloseCrt;
        Move(CrtNameID, Name, Sizeof(Name));
    end;
end;

function KeyPressed : boolean; assembler;
asm
	mov  	ah, $01
	int  	$16
	mov  	al, FALSE
	jz   	@@Done
	mov  	al, TRUE
@@Done:
	xor		ah, ah
end;

function ReadKey : char; assembler;
asm
	mov  	ah, $00
	int  	$16
	xor 	ah, ah
end;

procedure TextMode(Mode : word); assembler;
asm
	mov 	ax, Mode
	push	ax
	xor		ah, ah
	int		$10
	pop		ax
	test	ah,ah
	jz		@@DefaultFont
	mov		al, $12
	cmp		ah, 1
{$IFDEF Extras}
	je		@@LoadROMFont
	mov		al, $11
	cmp		ah, 2
	je		@@LoadROMFont
	mov		al, $14
	cmp		ah, 3
{$ENDIF}
	jne		@@DefaultFont
@@LoadROMFont:
	mov		ah, $11
	xor		bl, bl
	int		$10
@@DefaultFont:
	call    InitVideoData;
end;

procedure Window(X1, Y1, X2, Y2 : byte);
begin
	WindMin := Y1 shl 8 + X1;
	WindMax := Y2 shl 8 + X2;
	GotoXY(1,1);
end;

procedure GotoXY(X, Y : byte); assembler;
asm
	mov		ax, WindMin
	mov		dl, X
	mov		dh, Y
	sub		ax, $0101
	sub		dx, $0101
	add		dx, ax
	mov		ah, $02
	mov		bh, ActivePage
	int     $10
end;

function WhereX: byte; assembler;
asm
	mov		ah, $03
	mov		bh, ActivePage
	int		$10
	xor		ah, ah
	mov		al, dl
	inc		al
	mov		dx, WindMin
	sub		al, dl
	inc		al
end;

function WhereY: byte; assembler;
asm
	mov		ah, $03
	mov		bh, ActivePage
	int		$10
	xor		ah, ah
	mov		al, dh
	inc		al
	mov		dx, WindMin
	sub		al, dh
	inc		al
end;

procedure ClrScr; assembler;
asm
	mov		ax, $0600
	mov		bh, TextAttr
	mov		cx, WindMin
	mov		dx, WindMax
	sub		cx, $0101
	sub		dx, $0101
	int		$10
	mov		ah, $02
	mov		bh, ActivePage
	mov		dx, cx
	int		$10
end;

procedure ClrEol; assembler;
asm
	mov		ah, $03
	mov		bh, ActivePage
	int		$10
	mov		cx, WindMax
	xor		ch, ch
	sub		cl, dl
	test	cx, cx
	jz		@@Done
	mov		ax, $0920
	mov		bl, TextAttr
	int		$10
@@Done:
end;

procedure InsLine; assembler;
asm
	mov		ah, $03
	mov		bh, ActivePage
	int		$10
	mov		bx, dx
	mov		ax, $0701
	mov		cx, WindMin
	mov		dx, WindMax
	sub		cl, $01
	mov		ch, bh
	sub		dx, $0101
	mov		bh, TextAttr
	int		$10
end;

procedure DelLine; assembler;
asm
	mov		ah, $03
	mov		bh, ActivePage
	int		$10
	mov		bx, dx
	mov		ax, $0601
	mov		cx, WindMin
	mov		dx, WindMax
	sub		cl, $01
	mov		ch, bh
	sub		dx, $0101
	mov		bh, TextAttr
	int		$10
end;

procedure TextColor(Color : byte); assembler;
asm
	mov		al, Color
	mov		ah, TextAttr
	and		al, $0f
	and		ah, $f0
	or		al, ah
	mov		TextAttr, al
end;

procedure TextBackground(Color : byte); assembler;
asm
	mov		al, Color
	mov		cl, 4
	shl		al, cl
	mov		ah, TextAttr
	and		ah, $0f
	or		al, ah
	mov		TextAttr, al
end;

procedure LowVideo; assembler;
asm
	mov		al, TextAttr
	and		al, 11110111b
	mov		TextAttr, al
end;

procedure HighVideo; assembler;
asm
	mov		al, TextAttr
	or		al, 00001000b
	mov		TextAttr, al
end;

procedure NormVideo; assembler;
asm
	mov		al, TextAttr
	and		al, 11110111b
	mov		ah, FirstAttr
	and		ah, 00001000b
	or		al, ah
	mov		TextAttr, al
end;

{$IFDEF IntTimer}
procedure Delay(MS : word); assembler;
{ Simple Timer Tick Based Timer }
asm
	mov  	dx, MS
	test	dx, dx
	jz		@@NoDelay
	push	dx
	mov  	ax, $8301
	int  	$15
	mov  	ax, $8300
	mov  	DelayData, al
	pop		dx
	mov  	cx, dx
	shl  	dx, $0a
	shr  	cx, $06
	push	ds
	pop		es
	mov		bx, offset DelayData
	int  	$15
@@Loop:
	mov  	al, DelayData
	test 	al, $80
	jz   	@@Loop
@@NoDelay:
end;
{$ELSE}
procedure Delay(MS : word); assembler;
{ Simple Timer Tick Based Timer }
asm
	mov  	dx, MS
	test	dx, dx
	jz		@@NoDelay
	mov		ax, dx
	xor  	dx, dx
	mov  	cx, 55
	div  	cx
	cmp		dx, 23
	jl		@@NoRoundUp
	inc		ax
@@NoRoundUp:
	cmp		ax, 0
	je		@@Done
	mov		cx, ax
	mov  	dx, $0040
	mov		es, dx
	mov		di, $006C
@@Loop:
	mov		dx, [ES:DI]
@@Wait:
	mov		ax, [ES:DI]
	cmp		dx, ax
	je		@@Wait
	loop	@@Loop
@@Done:
@@NoDelay:
end;
{$ENDIF}

procedure Sound(Hz : word); assembler;
asm
	mov  	cx, Hz
	cmp		cx, $0012
	jbe  	@@NoSound
	mov		dx, $0012
	mov		ax, $34dc
	div		cx
	jmp  	@@DoSound
@@NoSound:
	xor		ax, ax
@@DoSound:
	push	ax
	mov 	al, 10110110b
	mov 	dx, $0043
	out 	dx, al
	mov 	dx, $0042
	pop		ax
	out		dx, al
	mov		al, ah
	out		dx, al
	mov		dx, $0061
	in		al, dx
	mov		al, $03
	out		dx, al
end;

procedure NoSound; assembler;
asm
	mov  	dx, $0061
	in   	al, dx
	and  	al, 11111101b
	out  	dx, al
	mov  	al, 10110110b
	mov  	dx, $0043
	out  	dx, al
	mov  	dx, $0042
	xor  	al, al
	out  	dx, al
	out  	dx, al
end;

begin
    UnitInitialize;
end.